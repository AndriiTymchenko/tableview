//
//  DetailViewController.swift
//  Recepies
//
//  Created by Andrii Tymchenko on 3/31/16.
//  Copyright © 2016 Andrii Tymchenko. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    
    var recept : (id: Int, name: String, description: String, time: Int, img: String)?
    //= (id: 0, name: "null", description: "null", time: 0, img: "null")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = recept!.name
        self.title     = recept!.name

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
