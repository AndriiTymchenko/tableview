//
//  ViewController.swift
//  Recepies
//
//  Created by Andrii Tymchenko on 3/31/16.
//  Copyright © 2016 Andrii Tymchenko. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    //var recipes = ["Мясо по- таскански", "Мясо по-французки", "Макароны", "Пицца с грибами", "Суп из овощей"]

    var recipes = [(id: 1, name: "Мясо по-французки", description: "Very tasty", time: 25, img: "food2.jpg"), (id: 2, name: "Мясо по- таскански", description: "Very tasty", time: 25, img: "food2.jpg"),
        (id: 3, name: "Макароны",           description: "Very tasty", time: 65, img: "food2.jpg"),
        (id: 4, name: "Пицца с грибами",    description: "Very tasty", time: 20, img: "food2.jpg"),
        (id: 5, name: "Суп из овощей",      description: "Very tasty", time: 95, img: "food2.jpg")]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = "cell"
        var cell = tableView.dequeueReusableCellWithIdentifier(identifier)
        if cell == nil {
            
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: identifier)
        }
        cell!.textLabel!.text = recipes[indexPath.row].name
        cell!.imageView!.image = UIImage(named: recipes[indexPath.row].img)
        return cell!
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            let indexPath = tableView.indexPathForSelectedRow
            let destViewController = segue.destinationViewController as! DetailViewController
            destViewController.recept = recipes[(indexPath?.row)!]
        }
    }
    
    
    
    
}

